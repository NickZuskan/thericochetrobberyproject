﻿//enums used in the Engine
namespace Engine
{
    public enum EngineValueUIType { Local, Global }
    public enum SpawnUIOptions { FromData, Override, None }
}
